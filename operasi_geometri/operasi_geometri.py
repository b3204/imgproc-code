from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
import cv2
import numpy as np
from math import sin, cos, pi, floor


def imgeser(F, sx, sy):
    tinggi, lebar = F.shape
    G = np.zeros(F.shape)
    for y in range(tinggi):
        for x in range(lebar):
            xlama = x - (sx)
            ylama = y - (sy)
            if (xlama >= 1) and (xlama < lebar) and (ylama >= 1) and (ylama < tinggi):
                G[y, x] = F[ylama, xlama]
            else:
                G[y, x] = 0
    return G


def imrotasi(F, sudut):
    tinggi, lebar = F.shape
    G = np.zeros(F.shape)
    rad = pi * (sudut / 180)
    cosa = cos(rad)
    sina = sin(rad)

    for y in range(tinggi):
        for x in range(lebar):
            x2 = round(x * cosa + y * sina)
            y2 = round(y * cosa + x * sina)
            if 0 <= x2 < lebar and 0 <= y2 < tinggi:
                # print(y, x, y2, x2)
                G[y, x] = F[y2, x2]
    return G


def imscalling(F, sy, sx):
    tinggi, lebar = F.shape
    tinggi_baru = tinggi * sy
    lebar_baru = lebar * sx
    G = np.zeros((tinggi_baru, lebar_baru))

    for y in range(tinggi_baru):
        for x in range(lebar_baru):
            y2 = ((y - 1) / sy)
            x2 = ((x - 1) / sx)
            G[y, x] = F[floor(y2), floor(x2)]
    return G

def imcermin(F, Flag):
    tinggi, lebar = F.shape
    G = np.zeros(F.shape)

    if Flag == 'horisontal':
        # print(True)
        for y in range(tinggi):
            for x in range(lebar):
                x2 = lebar - x - 1
                y2 = y
                G[y, x] = F[y2, x2]
    elif Flag == 'vertikal':
        for y in range(tinggi):
            for x in range(lebar):
                x2 = x
                y2 = tinggi - y - 1
                G[y, x] = F[y2, x2]
    return G
