from operasi_geometri import *


#
# img = cv2.imread('images/gedung.tif', cv2.IMREAD_GRAYSCALE)
# imtran = imgeser(img, 45, -35)
# fig, axs = plt.subplots(1, 2, figsize=(10, 10))
# axs[0].imshow(img, cmap='gray', vmin=0, vmax=255)
# axs[0].set_title("Citra Gedung Asli")
# axs[1].imshow(imtran, cmap='gray', vmin=0, vmax=255)
# axs[1].set_title("Hasil Pergeseran")
# plt.show()


#
#
# img = cv2.imread('images/sungai.png', cv2.IMREAD_GRAYSCALE)
# imrot = imrotasi(img, 15)
#
# fig, axs = plt.subplots(1, 2, figsize=(10, 10))
# axs[0].imshow(img, cmap='gray', vmin=0, vmax=255)
# axs[0].set_title("citra awal")
# axs[1].imshow(imrot, cmap='gray', vmin=0, vmax=255)
# axs[1].set_title("citra hasil")
# plt.show()


# img = cv2.imread('images/lena128.tif', cv2.IMREAD_GRAYSCALE)
# scal = imscalling(img,3,3)
# fig, axs = plt.subplots(1, 2, figsize=(10, 10))
# axs[0].imshow(img, cmap='gray', vmin=0, vmax=255)
# axs[0].set_title("citra awal")
# axs[1].imshow(scal, cmap='gray', vmin=0, vmax=255)
# axs[1].set_title("citra hasil")
# plt.show()


F = cv2.imread('images/boneka.tif', cv2.IMREAD_GRAYSCALE)
G = imcermin(F, 'horisontal')
fig, axs = plt.subplots(1, 2, figsize=(10, 10))
axs[0].imshow(F, cmap='gray', vmin=0, vmax=255)
axs[0].set_title("citra awal")
axs[1].imshow(G, cmap='gray', vmin=0, vmax=255)
axs[1].set_title("citra hasil")
plt.show()
