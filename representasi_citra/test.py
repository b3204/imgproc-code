from representasi_citra_digital import *

# 1. Resolusi dan Kuantisasi Citra
# img = cv2.imread('images/absam.png', cv2.IMREAD_GRAYSCALE)
# px = img[10:15, 15:25]
# print(px)

# 2. Ragam dan Konversi Citra
# color_image = cv2.imread('/bangunan.png')
# RGB = cv2.cvtColor(color_image, cv2.COLOR_BGR2RGB)
#
# R = RGB[:, :, 0]
# G = RGB[:, :, 1]
# B = RGB[:, :, 2]

# fig, ax = plt.subplots(2, 2, figsize=(20, 10))
# ax[0, 0].set_title('Citra RGB')
# ax[0, 0].imshow(RGB)
# ax[0, 1].set_title('Citra R')
# ax[0, 1].imshow(R, cmap='gray')
# ax[1, 0].set_title('Citra G')
# ax[1, 0].imshow(G, cmap='gray')
# ax[1, 1].set_title('Citra B')
# ax[1, 1].imshow(B, cmap='gray')
#
# fig.tight_layout()
# plt.show()


img = cv2.imread('images/bunga.tif', cv2.IMREAD_GRAYSCALE)
rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

imgGray = imcolor2gray(rgb)
imBiner = imgray2biner(imgGray, 50)
# plt.imshow(img_biner,cmap='binary')

fig, ax = plt.subplots(1, 3, figsize=(25, 25))
ax[0].set_title('RGB')
ax[0].imshow(rgb)
ax[1].set_title('Grayscale')
ax[1].imshow(imgGray, cmap='gray')
ax[2].set_title('Biner')
ax[2].imshow(imBiner, cmap='binary')
# fig.tight_layout()
plt.show()
