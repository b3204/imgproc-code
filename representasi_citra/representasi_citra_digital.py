import cv2
import matplotlib.pyplot as plt
import numpy as np


def imcolor2gray(img):
    r, g, b = img[:, :, 0], img[:, :, 1], img[:, :, 2]
    imGray = 0.298936 * r + 0.587043 * g + 0.114021 * b
    return imGray


def imgray2biner(imgGray, a):
    ambang = a
    tinggi, lebar = imgGray.shape

    imBiner = np.zeros((tinggi, lebar), 'uint8')
    for baris in range(tinggi):
        for kolom in range(lebar):
            if imgGray[baris, kolom] < ambang:
                imBiner[baris, kolom] = 0
            else:
                imBiner[baris, kolom] = 255
    return imBiner
