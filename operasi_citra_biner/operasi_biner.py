import numpy as np


def tepibiner(F):
    F = F / 255
    tinggi, lebar = F.shape
    G = np.zeros(F.shape, dtype='uint8')
    for q in range(1, tinggi - 1):
        for p in range(1, lebar - 1):
            p0 = int(F[q, p + 1])
            p1 = int(F[q - 1, p + 1])
            p2 = int(F[q - 1, p])
            p3 = int(F[q - 1, p - 1])
            p4 = int(F[q, p - 1])
            p5 = int(F[q + 1, p - 1])
            p6 = int(F[q + 1, p])
            p7 = int(F[q + 1, p + 1])
            sigma = p0 + p1 + p2 + p3 + p4 + p5 + p6 + p7
            if sigma == 8:
                G[q, p] = 0
            else:
                G[q, p] = F[q, p]
            # print(p, p0, p1, p2, p3, p4, p5, p6, p7)
        # print("")
    return G


def get_contour(BW):
    BW = BW / 255
    tinggi, lebar = BW.shape
    # Kontur = np.zeros(BW.shape, dtype='uint8')
    Kontur = []

    DPC = np.array([0, 1, 2, 3, 4, 5, 6, 7])
    DCP = np.array([4, 5, 6, 7, 0, 1, 2, 3])
    XP = np.array([1, 1, 0, -1, -1, -1, 0, 1])
    YP = np.array([0, -1, -1, -1, 0, 1, 1, 1])
    x1 = 1
    y1 = 1
    selesai = False
    for baris in range(tinggi):
        for kolom in range(lebar):
            if BW[baris, kolom] == 1:
                y1 = baris
                x1 = kolom - 1
                selesai = True

                # Kontur[0, 0] = y1
                # Kontur[0, 1] = x1
                element = np.array([y1, x1])
                Kontur.append(element)
                break

        if selesai:
            break

    dcn = 0
    for i in range(4, 7):
        if BW[y1 + YP[i], x1 + XP[i]] == 0:
            dcn = i
            break

    yberikut = y1 + YP[dcn]
    xberikut = x1 + XP[dcn]

    while (yberikut != Kontur[0][0]) or (xberikut != Kontur[0][1]):
        element = np.array([yberikut, xberikut])
        # print(indeks, element)
        Kontur.append(element)
        dpc = dcn

        for r in range(8):
            dcp = DCP[dpc + 1]
            de = (dcp + r) % 8
            di = (dcp + r + 1) % 8

            try:
                cxe = xberikut + XP[de]
                cye = yberikut + YP[de]
                cxi = xberikut + XP[di]
                cyi = yberikut + YP[di]
            except Exception as e:
                print(e.message)

            if (BW[cye, cxe] == 0) and (BW[cyi, cxi] == 1):
                yberikut = cye
                xberikut = cxe
                break

        indeks = indeks + 1
    return Kontur


def berikut(x):
    return 7 if x == 0 else x - 1


def sebelum(x):
    s = 0 if x == 0 else x + 1
    if s < 2:
        s = 2
    elif s < 4:
        s = 4
    elif s < 6:
        s = 6
    else:
        s = 0
    return s


def delta_piksel(id):
    dx = 0
    dy = 0
    if id == 0:
        dx = 1
        dy = 0
    elif id == 1:
        dx = 1
        dy = -1
    elif id == 2:
        dx = 0
        dy = -1
    elif id == 3:
        dx = -1
        dy = -1
    elif id == 4:
        dx = -1
        dy = 0
    elif id == 5:
        dx = -1
        dy = 1
    elif id == 6:
        dx = 0
        dy = 1
    elif id == 7:
        dx = 1
        dy = 1
    return dy, dx


def inbound_tracing(BW):
    jum_baris, jum_kolom = BW.shape
    selesai = False
    b0 = np.zeros(2, dtype='uint8')
    b1 = np.zeros(2, dtype='uint8')
    for p in range(jum_baris):
        for q in range(jum_kolom):
            if BW[p, q] == 1:
                b0[0] = p
                b0[1] = q

                selesai = True
                break

        if selesai:
            break

    c0 = 4
    c1 = 0
    for p in range(8):
        dy, dx = delta_piksel(c0)
        if BW[b0[0] + dy, b0[1] + dx] == 1:
            b1[0] = b0[0] + dy
            b1[1] = b0[1] + dx

            c1 = sebelum(c0)
            break
        else:
            c0 = berikut(c0)

    Kontur = []
    Kontur.append(b0)
    Kontur.append(b1)

    n = 2

    b = b1
    c = c1

    while True:
        for p in range(8):
            dy, dx = delta_piksel(c)
            if BW[b[0] + dy, b[1] + dx] == 1:
                b[0] = b[0] + dy
                b[1] = b[1] + dx

                c = sebelum(c)

                n = n + 1
                # Kontur(n, 1) = b.y
                # Kontur(n, 2) = b.x
                Kontur.append(np.array([b[0], b[1]]))

                break
            else:
                c = berikut(c)

        if (b[0] == b0[0]) and (b[1] == b0[1]):
            break
    return Kontur


def chain_code(U):
    Kode = ["3", "2", "1", "4", "0", "0", "5", "6", "7"]
    xawal = U[0][1]
    yawal = U[0][0]
    kode_rantai = ""
    for p in range(1, len(U)):
        deltay = U[p, 0] - U(p - 1, 0)
        deltax = U[p, 1] - U(p - 1, 1)
        indeks = 3 * deltay + deltax * 5
        kode_rantai += kode_rantai + Kode[indeks]
    return kode_rantai, xawal, yawal
