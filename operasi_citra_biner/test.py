import cv2
from operasi_biner import *
import matplotlib.pyplot as plt
import numpy as np

# 1. Deteksi Tepi
# img = cv2.imread('images/daun_bin.tif', cv2.IMREAD_GRAYSCALE)
# G = tepibiner(img)
#
# fig, axes = plt.subplots(1, 2, figsize=(12, 12))
# ax = axes.ravel()
#
# ax[0].imshow(img, cmap='gray')
# ax[0].set_title("Citra Biner")
# ax[1].imshow(G, cmap='gray')
# ax[1].set_title("Hasil Deteksi Tepi")
# plt.show()

# 2. Get Contour
# img = cv2.imread('images/daun_bin.tif', cv2.IMREAD_GRAYSCALE)
# C = get_contour(img)
# D = np.zeros(img.shape, dtype='uint8')
# for p in C:
#     D[p[0], p[1]] = 1
#
# fig, axes = plt.subplots(1, 2, figsize=(12, 12))
# ax = axes.ravel()
#
# ax[0].imshow(img, cmap='gray')
# ax[0].set_title("Citra Biner")
# ax[1].imshow(D, cmap='gray')
# ax[1].set_title("Hasil Deteksi Tepi")
# plt.show()

# 3. Inbound Tracking
# D = np.array([
#     [0, 0, 0, 0, 0, 0],
#     [0, 1, 1, 1, 1, 0],
#     [0, 0, 1, 1, 1, 0],
#     [0, 1, 1, 1, 0, 0],
#     [0, 0, 1, 1, 1, 0],
#     [0, 0, 0, 0, 0, 0]
# ])
# P = inbound_tracing(D)
# for p in P:
#     print(p)

# 4. chain_code
Daun = cv2.imread('images/daun_bin.tif', cv2.IMREAD_GRAYSCALE)
Daun = Daun / 255
C = inbound_tracing(Daun)
[kode, x, y] = chain_code(C)
print(kode, x, y)
