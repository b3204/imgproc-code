import cv2
import numpy as np
from matplotlib import pyplot as plt


# Histogram Citra
def imhistogram(imGray):
    tinggi, lebar = imGray.shape
    Histog = np.zeros((256), dtype=np.uint64)
    for baris in range(0, tinggi):
        for kolom in range(0, lebar):
            Histog[imGray[baris, kolom]] += 1
    return Histog


# img = cv2.imread('images/innsbruck.tif', cv2.IMREAD_GRAYSCALE)
# hist = imhistogram(img)
# Horis = np.arange(0, 256)
#
# fig, ax = plt.subplots(1, 2, figsize=(13, 3))
# ax[0].imshow(img, cmap='gray')
# ax[1].bar(Horis, hist)
# plt.show()

# Kecerahan dan Kontras
imGray = cv2.imread('images/absam.png', cv2.IMREAD_GRAYSCALE)
C = imGray + 20


# hist = imhistogram(C)
# Horis = np.arange(0, 256)
#
# plot, ax = plt.subplots(2, 2, figsize=(20, 10))
#
# ax[0, 0].set_title('Citra Asli')
# ax[0, 0].imshow(imGray, cmap='gray')
#
# ax[0, 1].set_title('Citra Kecerahan ditambah 20')
# ax[0, 1].imshow(C, cmap='gray')
#
# histA = imhistogram(imGray)
# histB = imhistogram(C)
# Horis = np.arange(0, 256)
# ax[1, 0].set_title('Histogram Citra Asli')
# ax[1, 0].bar(Horis, histA)
# ax[1, 1].set_title('Histogram Citra Kecerahan ditambah 20')
# ax[1, 1].bar(Horis, histB)
# plt.show()

# imGray = cv2.imread('images/gembala.tif', cv2.IMREAD_GRAYSCALE)
# K = 2 * imGray
#
# hist = imhistogram(K)
# Horis = np.arange(0, 256)
#
# plot, ax = plt.subplots(2, 2, figsize=(20, 10))
#
# ax[0, 0].set_title('Citra Asli')
# ax[0, 0].imshow(imGray, cmap='gray')
#
# ax[0, 1].set_title('Citra Kontras K=2')
# ax[0, 1].imshow(K, cmap='gray')
#
# histA = imhistogram(imGray)
# histB = imhistogram(K)
# Horis = np.arange(0, 256)
# ax[1, 0].set_title('Histogram Citra Asli')
# ax[1, 0].bar(Horis, histA)
# ax[1, 1].set_title('Histogram Citra Kontras K=2')
# ax[1, 1].bar(Horis, histB)
# plt.show()

# Clipping
def imclipping(imGray, f1, f2):
    tinggi, lebar = imGray.shape
    img = imGray.copy()
    for baris in range(tinggi):
        for kolom in range(lebar):
            if img[baris, kolom] <= f1:
                img[baris, kolom] = 0
            elif img[baris, kolom] >= f2:
                img[baris, kolom] = 255
    return img


img = cv2.imread('images/ipomoea.tif', cv2.IMREAD_GRAYSCALE)
clip = imclipping(img, 30, 105)

plot, (subplot1, subplot2) = plt.subplots(1, 2, figsize=(20, 10))

subplot1.set_title('Citra Asli')
subplot1.imshow(img, cmap='gray')

subplot2.set_title('Citra imclipping')
subplot2.imshow(clip, cmap='gray')
plt.show()
