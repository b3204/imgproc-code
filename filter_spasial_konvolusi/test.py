from filter_spasial_konvolusi import *
from filter_spasial.filter_spasial import imderausaltpepper

# F = cv2.imread('images/bangunan.png', cv2.IMREAD_GRAYSCALE)
# H = np.array([[-1, 0, -1], [0, 4, 0], [-1, 0, -1]])
# konv = konvolusi(F, H)
# fig, axs = plt.subplots(1, 2, figsize=(10, 10))
# axs[0].imshow(F, cmap='gray', vmin=0, vmax=255)
# axs[0].set_title("Citra Input")
# axs[1].imshow(konv, cmap='gray', vmin=0, vmax=255)
# axs[1].set_title("Citra Hasil Konvolusi")
# plt.show()

F = cv2.imread('images/boneka.tif', cv2.IMREAD_GRAYSCALE)
F1 = imderausaltpepper(F, 0.3)
F2 = cv2.imread('images/goldhill.tif', cv2.IMREAD_GRAYSCALE)
H = np.array([[1, 1, 1],
              [1, 1, 1],
              [1, 1, 1]]) / 9
G = konvolusi(F1, H)
G2 = konvolusi(F2, H)
fig, axes = plt.subplots(2, 2, figsize=(12, 12))
ax = axes.ravel()

ax[0].imshow(F1, cmap='gray')
ax[0].set_title("Citra boneka yang dilengkapi derau")
ax[1].imshow(G, cmap='gray')
ax[1].set_title("Hasil penapisan citra boneka")
ax[2].imshow(F2, cmap='gray')
ax[2].set_title("Citra Goldhill")
ax[3].imshow(G2, cmap='gray')
ax[3].set_title("Hasil penapisan citra goldhill")
plt.show()
