import cv2
import numpy as np
import matplotlib.pyplot as plt
from math import floor


def konvolusi(F, H):
    tinggi_f, lebar_f = F.shape
    tinggi_h, lebar_h = H.shape

    m2 = floor(tinggi_h / 2)
    n2 = floor(lebar_h / 2)

    F2 = np.copy(F).astype('double')
    G = np.zeros(F.shape, dtype='int')

    for y in range(m2, tinggi_f - m2):
        for x in range(n2, lebar_f - n2):
            jum = 0
            for p in range(-m2, m2 + 1):
                for q in range(-n2, n2 + 1):
                    jum = jum + H[p + m2, q + n2] * F2[y - p, x - q]
                    # print("norm: ", jum, (p + m2), (q + n2), (y - p), (x - q))
                # print("")
            G[y - m2, x - n2] = jum

    return G