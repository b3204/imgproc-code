from filter_spasial import *

# F = cv2.imread('images/innsbruck.tif', cv2.IMREAD_GRAYSCALE)
# gaus1 = imderaugaussian(F, 10)
# gaus2 = imderaugaussian(F, 25)
# gaus3 = imderaugaussian(F, 50)
#
# fig, axs = plt.subplots(2, 2, figsize=(15, 15))
# axs[0, 0].imshow(F, cmap='gray')
# axs[0, 0].set_title("Citra Awal")
# axs[0, 1].imshow(gaus1, cmap='gray')
# axs[0, 1].set_title("Citra Noise sigma 10")
# axs[1, 0].imshow(gaus2, cmap='gray')
# axs[1, 0].set_title("Citra Noise sigma 25")
# axs[1, 1].imshow(gaus3, cmap='gray')
# axs[1, 1].set_title("Citra Noise sigma 50")
# plt.show()


# image = cv2.imread('images/innsbruck.tif', 0)  # Only for grayscale image
# noise_img1 = imderausaltpepper(image, 0.01)
# noise_img2 = imderausaltpepper(image, 0.025)
# noise_img3 = imderausaltpepper(image, 0.05)
#
# fig, axs = plt.subplots(2, 2, figsize=(15, 15))
# axs[0, 0].imshow(image, cmap='gray')
# axs[0, 0].set_title("Citra Awal")
# axs[0, 1].imshow(noise_img1, cmap='gray')
# axs[0, 1].set_title("Citra Noise prob 0.01")
# axs[1, 0].imshow(noise_img2, cmap='gray')
# axs[1, 0].set_title("Citra Noise prob 0.025")
# axs[1, 1].imshow(noise_img3, cmap='gray')
# axs[1, 1].set_title("Citra Noise prob 0.05")
# plt.show()


# paper = cv2.imread('images/innsbruck.tif', cv2.IMREAD_GRAYSCALE)
# paper1 = imderaueksponensial(paper, 0.1)
# paper2 = imderaueksponensial(paper, 0.07)
# paper3 = imderaueksponensial(paper, 0.01)
#
# fig, axs = plt.subplots(2, 2, figsize=(15, 15))
# axs[0, 0].imshow(paper, cmap='gray')
# axs[0, 0].set_title("Citra Awal")
# axs[0, 1].imshow(paper1, cmap='gray')
# axs[0, 1].set_title("Citra Noise a = 0.1")
# axs[1, 0].imshow(paper2, cmap='gray')
# axs[1, 0].set_title("Citra Noise a = 0.07")
# axs[1, 1].imshow(paper3, cmap='gray')
# axs[1, 1].set_title("Citra Noise a = 0.01")
# plt.show()


# F = cv2.imread('images/mobil.tif', cv2.IMREAD_GRAYSCALE)
# F1 = cv2.imread('images/boneka.tif', cv2.IMREAD_GRAYSCALE)
# G = imfilbatas(F)
# G1 = imfilbatas(F1)
# fig, axes = plt.subplots(2, 2, figsize=(10, 10))
# ax = axes.ravel()
#
# ax[0].imshow(F, cmap='gray')
# ax[0].set_title("Input Citra 1")
#
# ax[1].imshow(G, cmap='gray')
# ax[1].set_title("Output Citra 1")
#
# ax[2].imshow(F1, cmap='gray')
# ax[2].set_title("Input Citra 2")
#
# ax[3].imshow(G1, cmap='gray')
# ax[3].set_title("Output Citra 2")
# plt.show()


# F = cv2.imread('images/mobil.tif', cv2.IMREAD_GRAYSCALE)
# G = imfilrerata(F)
# F1 = cv2.imread('images/boneka.tif', cv2.IMREAD_GRAYSCALE)
# G1 = imfilrerata(F1)
# fig, axes = plt.subplots(2, 2, figsize=(10, 10))
# ax = axes.ravel()
#
# ax[0].imshow(F, cmap='gray')
# ax[0].set_title("Input Citra 1")
#
# ax[1].imshow(G, cmap='gray')
# ax[1].set_title("Output Citra 1")
#
# ax[2].imshow(F1, cmap='gray')
# ax[2].set_title("Input Citra 2")
#
# ax[3].imshow(G1, cmap='gray')
# ax[3].set_title("Output Citra 2")
#
# plt.show()


F = cv2.imread('images/mobil.tif', cv2.IMREAD_GRAYSCALE)
G = imfilmedian(F)
F1 = cv2.imread('images/boneka.tif', cv2.IMREAD_GRAYSCALE)
G1 = imfilmedian(F1)
fig, axes = plt.subplots(2, 2, figsize=(10, 10))
ax = axes.ravel()
ax[0].imshow(F, cmap='gray')
ax[0].set_title("Input Citra 1")

ax[1].imshow(G, cmap='gray')
ax[1].set_title("Output Citra 1")

ax[2].imshow(F1, cmap='gray')
ax[2].set_title("Input Citra 2")

ax[3].imshow(G1, cmap='gray')
ax[3].set_title("Output Citra 2")
plt.show()
