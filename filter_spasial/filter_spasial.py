import cv2
import numpy as np
import matplotlib.pyplot as plt
import random, math


def imderaugaussian(F, sigma, mu=None):
    nargin = 0
    if nargin < 3:
        mu = 0
    elif nargin < 2:
        sigma = 1
    G = np.zeros(F.shape, np.uint8)
    for i in range(F.shape[0]):
        for j in range(F.shape[1]):
            rdn = np.random.random()
            derau = rdn * sigma + mu
            G[i][j] = round(F[i][j] + derau)
            if G[i][j] > 255:
                G[i][j] = 255
            elif G[i][j] < 0:
                G[i][j] = 0
            else:
                G[i][j] = F[i][j]
    return G


# F = cv2.imread('images/innsbruck.tif', cv2.IMREAD_GRAYSCALE)
# gaus1 = imderaugaussian(F, 10)
# gaus2 = imderaugaussian(F, 25)
# gaus3 = imderaugaussian(F, 50)
#
# fig, axs = plt.subplots(2, 2, figsize=(15, 15))
# axs[0, 0].imshow(F, cmap='gray')
# axs[0, 0].set_title("Citra Awal")
# axs[0, 1].imshow(gaus1, cmap='gray')
# axs[0, 1].set_title("Citra Noise sigma 10")
# axs[1, 0].imshow(gaus2, cmap='gray')
# axs[1, 0].set_title("Citra Noise sigma 25")
# axs[1, 1].imshow(gaus3, cmap='gray')
# axs[1, 1].set_title("Citra Noise sigma 50")
# plt.show()

def imderausaltpepper(F, probabilitas):
    G = np.zeros(F.shape, np.uint8)
    for i in range(F.shape[0]):
        for j in range(F.shape[1]):
            nilai_acak = random.random()
            if nilai_acak <= probabilitas / 2:
                G[i][j] = 0
            elif (nilai_acak > probabilitas / 2) and (nilai_acak <= probabilitas):
                G[i][j] = 255
            else:
                G[i][j] = F[i][j]

    return G


# image = cv2.imread('images/innsbruck.tif', 0)  # Only for grayscale image
# noise_img1 = imderausaltpepper(image, 0.01)
# noise_img2 = imderausaltpepper(image, 0.025)
# noise_img3 = imderausaltpepper(image, 0.05)
#
# fig, axs = plt.subplots(2, 2, figsize=(15, 15))
# axs[0, 0].imshow(image, cmap='gray')
# axs[0, 0].set_title("Citra Awal")
# axs[0, 1].imshow(noise_img1, cmap='gray')
# axs[0, 1].set_title("Citra Noise prob 0.01")
# axs[1, 0].imshow(noise_img2, cmap='gray')
# axs[1, 0].set_title("Citra Noise prob 0.025")
# axs[1, 1].imshow(noise_img3, cmap='gray')
# axs[1, 1].set_title("Citra Noise prob 0.05")
# plt.show()

def imderaueksponensial(F, a):
    G = np.zeros(F.shape, np.uint8)
    for i in range(F.shape[0]):
        for j in range(F.shape[1]):
            rand = random.random()
            derau = -1 / a * math.log(1 - rand)
            G[i][j] = round(F[i][j] + derau)
            if G[i][j] > 255:
                G[i][j] = 255
    return G


# paper = cv2.imread('images/innsbruck.tif', cv2.IMREAD_GRAYSCALE)
# paper1 = imderaueksponensial(paper, 0.1)
# paper2 = imderaueksponensial(paper, 0.07)
# paper3 = imderaueksponensial(paper, 0.01)
#
# fig, axs = plt.subplots(2, 2, figsize=(15, 15))
# axs[0, 0].imshow(paper, cmap='gray')
# axs[0, 0].set_title("Citra Awal")
# axs[0, 1].imshow(paper1, cmap='gray')
# axs[0, 1].set_title("Citra Noise a = 0.1")
# axs[1, 0].imshow(paper2, cmap='gray')
# axs[1, 0].set_title("Citra Noise a = 0.07")
# axs[1, 1].imshow(paper3, cmap='gray')
# axs[1, 1].set_title("Citra Noise a = 0.01")
# plt.show()


def imfilbatas(F):
    tinggi, lebar = F.shape
    G = np.zeros(F.shape)

    for baris in range(0, tinggi - 1):
        for kolom in range(0, lebar - 1):

            minPiksel = min(F[baris - 1, kolom - 1], F[baris - 1, kolom], F[baris, kolom + 1],
                            F[baris, kolom - 1], F[baris, kolom + 1], F[baris + 1, kolom - 1],
                            F[baris + 1, kolom], F[baris + 1, kolom + 1])
            maksPiksel = max(F[baris - 1, kolom - 1], F[baris - 1, kolom], F[baris, kolom + 1],
                             F[baris, kolom - 1], F[baris, kolom + 1], F[baris + 1, kolom - 1],
                             F[baris + 1, kolom], F[baris + 1, kolom + 1])

            if F[baris, kolom] < minPiksel:
                G[baris, kolom] = minPiksel
            else:
                if F[baris, kolom] > maksPiksel:
                    G[baris, kolom] = maksPiksel
                else:
                    G[baris, kolom] = F[baris, kolom]

    return G


# F = cv2.imread('images/mobil.tif', cv2.IMREAD_GRAYSCALE)
# F1 = cv2.imread('images/boneka.tif', cv2.IMREAD_GRAYSCALE)
# G = imfilbatas(F)
# G1 = imfilbatas(F1)
# fig, axes = plt.subplots(2, 2, figsize=(10, 10))
# ax = axes.ravel()
#
# ax[0].imshow(F, cmap='gray')
# ax[0].set_title("Input Citra 1")
#
# ax[1].imshow(G, cmap='gray')
# ax[1].set_title("Output Citra 1")
#
# ax[2].imshow(F1, cmap='gray')
# ax[2].set_title("Input Citra 2")
#
# ax[3].imshow(G1, cmap='gray')
# ax[3].set_title("Output Citra 2")
# plt.show()

def imfilrerata(F):
    G = np.zeros(F.shape, dtype='uint8')
    tinggi, lebar = F.shape

    for baris in range(0, tinggi - 1):
        for kolom in range(0, lebar - 1):
            jumlah = int(F[baris - 1, kolom - 1]) + int(F[baris - 1, kolom]) + int(F[baris - 1, kolom - 1]) + \
                     int(F[baris, kolom - 1]) + int(F[baris, kolom] + F[baris, kolom + 1]) + \
                     int(F[baris + 1, kolom - 1]) + int(F[baris + 1, kolom]) + int(F[baris + 1, kolom + 1])
            G[baris, kolom] = (1 / 9 * jumlah)
    return G


# F = cv2.imread('images/mobil.tif', cv2.IMREAD_GRAYSCALE)
# G = imfilrerata(F)
# F1 = cv2.imread('images/boneka.tif', cv2.IMREAD_GRAYSCALE)
# G1 = imfilrerata(F1)
# fig, axes = plt.subplots(2, 2, figsize=(10, 10))
# ax = axes.ravel()
#
# ax[0].imshow(F, cmap='gray')
# ax[0].set_title("Input Citra 1")
#
# ax[1].imshow(G, cmap='gray')
# ax[1].set_title("Output Citra 1")
#
# ax[2].imshow(F1, cmap='gray')
# ax[2].set_title("Input Citra 2")
#
# ax[3].imshow(G1, cmap='gray')
# ax[3].set_title("Output Citra 2")
#
# plt.show()

def imfilmedian(F):
    tinggi, lebar = F.shape
    G = np.zeros(F.shape, dtype='uint8')

    for baris in range(0, tinggi - 1):
        for kolom in range(0, lebar - 1):

            data = [F[baris - 1, kolom - 1], F[baris - 1, kolom], F[baris, kolom + 1], F[baris, kolom - 1],
                    F[baris, kolom], F[baris, kolom + 1], F[baris + 1, kolom - 1], F[baris + 1, kolom],
                    F[baris + 1, kolom + 1]]

            for i in range(1, 8):
                for j in range(1, 9):
                    if data[i] > data[j]:
                        tmp = data[i]
                        data[i] = data[j]
                        data[j] = tmp
            G[baris, kolom] = data[5]
    return G


# F = cv2.imread('images/mobil.tif', cv2.IMREAD_GRAYSCALE)
# G = imfilmedian(F)
# F1 = cv2.imread('images/boneka.tif', cv2.IMREAD_GRAYSCALE)
# G1 = imfilmedian(F1)
# fig, axes = plt.subplots(2, 2, figsize=(10, 10))
# ax = axes.ravel()
# ax[0].imshow(F, cmap='gray')
# ax[0].set_title("Input Citra 1")
#
# ax[1].imshow(G, cmap='gray')
# ax[1].set_title("Output Citra 1")
#
# ax[2].imshow(F1, cmap='gray')
# ax[2].set_title("Input Citra 2")
#
# ax[3].imshow(G1, cmap='gray')
# ax[3].set_title("Output Citra 2")
# plt.show()
