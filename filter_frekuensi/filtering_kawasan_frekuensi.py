from math import sin, cos, pi

import cv2
import numpy as np
import time


def current_milli_time():
    return round(time.time() * 1000);


def dft2d(F):
    Fx = F.astype('double')
    m, n = F.shape

    t_now = current_milli_time();
    for v in range(m):
        for u in range(n):
            Re = np.zeros(Fx.shape, dtype='double')
            Im = np.zeros(Fx.shape, dtype='double')
            for y in range(m):
                for x in range(n):
                    radian = 2 * pi * (u * x / n + v * y / m)
                    cosr = cos(radian)
                    sinr = -sin(radian)
                    Re[v, u] = Re[v, u] + Fx[y, x] * cosr
                    Im[v, u] = Im[v, u] + Fx[y, x] * sinr
                    # print(x, round(radian), round(cosr), round(sinr), Re[v, u], Im[v, u])
                # print("")
    t_finished = current_milli_time() - t_now
    return t_finished * 1000  # ms


def fft2(F):
    img_float32 = np.float32(F)

    dft = cv2.dft(img_float32, flags=cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)

    magnitude_spectrum_a = 20 * np.log(cv2.magnitude(dft[:, :, 0], dft[:, :, 1]))
    magnitude_spectrum_b = 20 * np.log(cv2.magnitude(dft_shift[:, :, 0], dft_shift[:, :, 1]))

    return magnitude_spectrum_a, magnitude_spectrum_b
