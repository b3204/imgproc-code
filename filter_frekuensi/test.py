from filtering_kawasan_frekuensi import dft2d, fft2
import cv2
from matplotlib import pyplot as plt

img = cv2.imread('images/lena256.tif', cv2.IMREAD_GRAYSCALE)
magnitude_spectrum, magnitude_spectrum1 = fft2(img)
fig, axs = plt.subplots(1, 3, figsize=(15, 15))
axs[0].imshow(img, cmap='gray', vmin=0, vmax=255)
axs[0].set_title("Citra Lena 256")
axs[1].imshow(magnitude_spectrum, cmap='gray', vmin=0, vmax=255)
axs[1].set_title("Skala Logaritmic")
axs[2].imshow(magnitude_spectrum1, cmap='gray', vmin=0, vmax=255)
axs[2].set_title("Freq. nol ditengahkan")
plt.show()
